﻿using System.Collections.Generic;
using UnityEngine;
using System.Collections;

public class ObjectPool
{
    public event SpawnEvent OnObjectSpawn;
    public event SpawnEvent OnObjectDeSpawn;

    public List<GameObject> ActiveObject { get; set; }
    private Queue<GameObject> DespawnedElements { get; set; }
    private GameObject Prefab { get; set; }
    private Transform Parent { get; set; }

    public ObjectPool(GameObject prefab, Transform parent, int preallocateAmount = 0)
    {
        Prefab = prefab;
        Parent = parent;
        ActiveObject = new List<GameObject>();
        DespawnedElements = new Queue<GameObject>();
    }

    public GameObject Spawn()
    {
        return Spawn(Vector3.zero);
    }

    public GameObject Spawn(Vector3 pos)
    {
        return Spawn(pos, Quaternion.identity);
    }

    public GameObject Spawn(Vector3 pos, Quaternion rot)
    {
        GameObject com = null;
        if (DespawnedElements.Count != 0)
        {
            com = DespawnedElements.Dequeue();
        }
        else
        {
            com = Instantiate();
        }
        com.transform.position = pos;
        com.transform.transform.rotation = rot;
        ActiveObject.Add(com);
        com.gameObject.SetActive(true);
        if (OnObjectSpawn != null)
        {
            OnObjectSpawn.Invoke(com);
        }
        return com;
    }

    public bool Despawn(GameObject pObject, bool DestroyObject = false)
    {
        if (ActiveObject.Remove(pObject))
        {
            if (OnObjectDeSpawn != null)
            {
                OnObjectDeSpawn.Invoke(pObject);
            }
            if (DestroyObject)
            {
                Destroy(pObject);
            }
            else
            {
                pObject.SetActive(false);
                DespawnedElements.Enqueue(pObject);
            }
            return true;
        }
        return false;
    }

    public bool DespawnAllActiveObjects(bool DestroyObject = false)
    {
        while (ActiveObject.Count != 0)
        {
            if (!Despawn(ActiveObject[0], DestroyObject))
            {
                return false;
            }

        }
        return true;
    }

    private GameObject Instantiate()
    {
        GameObject go = (GameObject)GameObject.Instantiate(Prefab);
        if (Parent != null)
        {
            go.transform.parent = Parent;
        }
        return go;
    }

    private void Destroy(GameObject pObject)
    {
        GameObject.Destroy(pObject);
    }

    public void Allocate(int amount)
    {
        for (int i = 0; i < amount; i++)
        {
            GameObject com = (GameObject)GameObject.Instantiate(Prefab);
            com.gameObject.SetActive(false);
            if (Parent != null)
            {
                com.transform.parent = Parent;
            }
            DespawnedElements.Enqueue(com);
        }
    }

    public void DeAllocate(int amount)
    {
        for (int i = 0; i < amount; i++)
        {
            if (DespawnedElements.Count == 0)
                break;

            Destroy(DespawnedElements.Dequeue());
        }
    }
}

public delegate void SpawnEvent(GameObject targetGameObject);
public delegate void DeSpawnEvent(GameObject targetGameObject);