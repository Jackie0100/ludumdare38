﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class Chip : MonoBehaviour
{
    [SerializeField]
    bool _isCorrupted;
    [SerializeField]
    Font _normalFont;
    [SerializeField]
    Font _corruptedFont;
    [SerializeField]
    Text _chipText;
    [SerializeField]
    Image _progressbar;

    [SerializeField]
    float _updateTime = 1;

    float _timer;

    [SerializeField]
    float _captureTime = 10;

    float _captureCounter = 0;
    bool _iscapturing;

    public UnityEvent OnCaptureComplete;

    public float CaptureCounter
    {
        get
        {
            return _captureCounter;
        }

        set
        {
            if (value <= 0)
            {
                _captureCounter = 0;
            }
            else
            {
                _captureCounter = value;
                if (value >= _captureTime && !IsCorrupted)
                {
                    if (OnCaptureComplete != null)
                    {
                        OnCaptureComplete.Invoke();
                    }
                    IsCorrupted = true;
                }
            }
        }
    }

    public bool IsCorrupted
    {
        get
        {
            return _isCorrupted;
        }

        set
        {
            if (value)
            {
                _chipText.font = _corruptedFont;
            }
            else
            {
                _chipText.font = _normalFont;
            }
            _chipText.text = GenerateText(TextGenerationOptions.HexaDecimal, 200);
            _timer = 0;
            _isCorrupted = value;
        }
    }

    public bool Iscapturing
    {
        get
        {
            return _iscapturing;
        }

        set
        {
            _progressbar.transform.parent.parent.gameObject.SetActive(value);
            _iscapturing = value;
        }
    }

    private void Start()
    {
        Iscapturing = false;
        IsCorrupted = _isCorrupted;
        _chipText.text = GenerateText(TextGenerationOptions.HexaDecimal, 200);
    }

    // Update is called once per frame
    void Update()
    {
        _timer += Time.deltaTime;

        if (_timer > _updateTime)
        {
            if (_isCorrupted)
            {
                _chipText.text = GenerateText(TextGenerationOptions.Text, 200);
            }
            else
            {
                _chipText.text = GenerateText(TextGenerationOptions.HexaDecimal, 200);
            }
            _timer = 0;
        }

        if (Iscapturing)
        {
            CaptureCounter += Time.deltaTime;
            _progressbar.fillAmount = _captureCounter / _captureTime;
        }
        else if (CaptureCounter < _captureTime)
        {
            CaptureCounter -= (Time.deltaTime * 0.25f);
        }
    }

    public void Corrupt()
    {
        CaptureCounter = _captureTime + 1;
    }

    private string GenerateText(TextGenerationOptions tgo, int characterAmount)
    {
        string str = "";
        for (int i = 0; i < characterAmount; i++)
        {
            switch (tgo)
            {
                case TextGenerationOptions.Text:
                    str += (char)(65 + Random.Range(0, 25));

                    break;
                case TextGenerationOptions.HexaDecimal:
                default:
                    if (Random.Range(0, 4) >= 1)
                    {
                        str += Random.Range(0, 10);
                    }
                    else
                    {
                        str += (char)(65 + Random.Range(0, 6));
                    }
                    break;
            }
        }
        return str;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player" || (collision.GetComponent<Enemy>() != null && collision.GetComponent<Enemy>().AiState == AIStates.Converted))
        {
            Iscapturing = true;
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.tag == "Player" || (collision.GetComponent<Enemy>() != null && collision.GetComponent<Enemy>().AiState == AIStates.Converted))
        {
            Iscapturing = true;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "Player" || (collision.GetComponent<Enemy>() != null && collision.GetComponent<Enemy>().AiState == AIStates.Converted))
        {
            Iscapturing = false;
        }
    }
}

public enum TextGenerationOptions
{
    HexaDecimal,
    Text,
    Numbers,
    Binary,
}