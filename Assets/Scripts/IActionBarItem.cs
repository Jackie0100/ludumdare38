﻿using UnityEngine;

public interface IActionBarItem
{
    Sprite Icon { get; set; }
    float CooldownTimer { get; set; }
    float CooldownTime { get; set; }
    //TODO: Make alternative sprite/coller for attacks that is trigger on/off

    void UseItem(Player player);
    bool DoesHaveEnoughPower(Player player);
    bool IsUsable(Player player);
}