﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PackageCorruption : Attack
{
    Coroutine powerdrain;
    public override void DoAttack(Player player)
    {
        player.IsInvissibleActive = !player.IsInvissibleActive;

        if (player.IsInvissibleActive)
        {
            player.Power -= _powerCost;
            powerdrain = player.StartCoroutine(DrainPower(player, 1));
            player.PlaySoundLoop(_attackSounds[UnityEngine.Random.Range(0, _attackSounds.Length)]);
        }
        else
        {
            player.StopCoroutine(powerdrain);
            player.StopSound(_attackSounds[UnityEngine.Random.Range(0, _attackSounds.Length)]);
        }
    }
}
