﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[System.Serializable]
public class StackOverflow : Attack
{
    [SerializeField]
    float _empowerment = 300;
    public override void DoAttack(Player player)
    {
        base.DoAttack(player);

        Collider2D[] hitinfo = Physics2D.OverlapCircleAll(player.transform.position, 10, 1);

        if (hitinfo.Length > 0)
        {
            float dist = 100;
            Transform _target = player.transform;
            foreach (Collider2D col in hitinfo)
            {
                if (col.GetComponent<Enemy>() == null)
                    continue;
                if (col.GetComponent<Enemy>().AiState == AIStates.Converted)
                    continue;

                if (Vector2.Distance(col.transform.position, player.transform.position) < dist)
                {
                    dist = Vector2.Distance(col.transform.position, player.transform.position);
                    _target = col.transform;
                }
            }
            _target.GetComponent<Enemy>().Convert(_empowerment);
        }
    }

    public override bool IsUsable(Player player)
    {
        Collider2D[] hitinfo = Physics2D.OverlapCircleAll(player.transform.position, 10, 1);
        return hitinfo.Any(c => c.GetComponent<Enemy>() != null && c.GetComponent<Enemy>().AiState != AIStates.Converted);
    }
}
