﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class BackDoor : Attack
{
    [SerializeField]
    float _empowerment;
    [SerializeField]
    float _health;
    public override void DoAttack(Player player)
    {
        base.DoAttack(player);
        GameObject go = GameObject.Instantiate<GameObject>(_bulletObject, player.transform.position, Quaternion.identity);
        go.GetComponent<BackDoorAI>().startEmpowerment = _empowerment;
        go.GetComponent<BackDoorAI>().Health = _health;
    }
}
