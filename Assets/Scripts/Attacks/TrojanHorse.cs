﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class TrojanHorse : Attack
{
    public override void DoAttack(Player player)
    {
        base.DoAttack(player);

        GameObject.Instantiate<GameObject>(_bulletObject, player.transform, false);
    }
}
