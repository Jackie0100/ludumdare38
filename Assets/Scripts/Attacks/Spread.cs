﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Spread : Attack
{
    [SerializeField]
    public int _maxAOESpreadAmount;
    [SerializeField]
    public float _startEmpowerment;

    public override void DoAttack(Player player)
    {
        base.DoAttack(player);

        GameObject go;
        for (int i = 0; i < _maxAOESpreadAmount; i++)
        {
            go = GameObject.Instantiate<GameObject>(_bulletObject, player.transform.position, Quaternion.Euler(0, 0, (360.0f / (float)_maxAOESpreadAmount) * i));
            go.GetComponent<SpreadAI>().empowerment = _startEmpowerment;
        }
    }
}
