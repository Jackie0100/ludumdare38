﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ForkBomb : Attack
{
    [SerializeField]
    float _stunTime;

    public override void DoAttack(Player player)
    {
        base.DoAttack(player);

        GameObject[] gos = GameObject.FindGameObjectsWithTag("Enemy");

        foreach (GameObject go in gos)
        {
            go.GetComponent<Enemy>().StunEnemy(_stunTime);
        }
    }
}
