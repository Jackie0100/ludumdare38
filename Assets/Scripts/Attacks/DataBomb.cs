﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class DataBomb : Attack
{
    [SerializeField]
    public Sprite normalSprite;
    [SerializeField]
    public Sprite blinkSprite;
    [SerializeField]
    public float explosionTime;

    public override void DoAttack(Player player)
    {
        base.DoAttack(player);

        GameObject go = GameObject.Instantiate<GameObject>(_bulletObject, player.transform.position, Quaternion.identity);
        go.GetComponent<DataBombAI>().dataBomb = this;
    }
}
