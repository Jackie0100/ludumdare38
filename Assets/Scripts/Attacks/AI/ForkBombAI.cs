﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ForkBombAI : MonoBehaviour
{
    [SerializeField]
    float _destructionTime = 10;

    // Use this for initialization
    void Start ()
    {
        Invoke("DestroyObject", _destructionTime);
    }

    // Update is called once per frame
    void Update ()
    {
		
	}

    private void DestroyObject()
    {
        Destroy(this.gameObject);
    }
}
