﻿using System;
using UnityEngine;

public class SpreadAI : MonoBehaviour
{
    [SerializeField]
    float _translateSpeed = 10;
    [SerializeField]
    float _destructionTime = 10;
    [SerializeField]
    public float empowerment = 100;


    private void Start()
    {
        Invoke("DestroyObject", _destructionTime);
    }

    private void Update()
    {
        this.transform.Translate(Vector3.up * Time.deltaTime * _translateSpeed);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Enemy")
        {
            collision.GetComponent<Enemy>().Convert(empowerment);
            Destroy(this.gameObject);
        }
    }

    private void DestroyObject()
    {
        Destroy(this.gameObject);
    }
}