﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrojanAI : MonoBehaviour
{
    [SerializeField]
    GameObject _trojanSprite;
    [SerializeField]
    float _destructionTime = 10;

    // Use this for initialization
    void Start()
    {
        Invoke("DestroyObject", _destructionTime);
        StartCoroutine(SpawnHorses());
    }

    IEnumerator SpawnHorses()
    {
        while (true)
        {
            GameObject go = GameObject.Instantiate<GameObject>(_trojanSprite, this.transform.parent, false);
            go.transform.position += new Vector3(Random.Range(-1.0f, 1.0f), Random.Range(-1.0f, 1.0f)).normalized * 50;
            go.transform.rotation = Quaternion.Euler(0, 0, (Mathf.Atan2(go.transform.position.y - this.transform.position.y, go.transform.position.x - this.transform.position.x) * 180 / Mathf.PI) - 180) ;//   Vector3.Dot(go.transform.position, this.transform.position) / (go.transform.position.magnitude * this.transform.position.magnitude)));

            yield return new WaitForSeconds(1);
        }
    }

    private void DestroyObject()
    {
        Destroy(this.gameObject);
    }
}
