﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackDoorAI : MonoBehaviour
{
    [SerializeField]
    GameObject _backdoorSpawn;

    float _health = 500;

    public float startEmpowerment;

    public float Health
    {
        get
        {
            return _health;
        }

        set
        {
            if (value <= 0)
            {
                Destroy(this.gameObject);
            }
            _health = value;
        }
    }

    // Use this for initialization
    void Start ()
    {
        StartCoroutine(SpawnVirus());
	}

    IEnumerator SpawnVirus()
    {
        GameObject go = this.gameObject;
        while (true)
        {
            yield return new WaitForSeconds(5);
            go = GameObject.Instantiate<GameObject>(_backdoorSpawn, this.transform.position, Quaternion.identity, GameObject.FindObjectOfType<EnemySpawner>().transform);
            go.GetComponent<Enemy>().Convert(startEmpowerment);
        }
        yield return null;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Enemy")
        {
            if (collision.transform.GetComponent<Enemy>().AiState != AIStates.Converted)
            {
                Health -= 100;
                Destroy(collision.gameObject);
            }
        }
    }
}
