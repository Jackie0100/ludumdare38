﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrojanMoverAI : MonoBehaviour
{
    [SerializeField]
    float _translateSpeed = 30;
    [SerializeField]
    float _destructionTime = 10;

    private void Start()
    {
        Invoke("DestroyObject", _destructionTime);
    }

    private void Update()
    {
        this.transform.Translate(Vector3.right * Time.deltaTime * _translateSpeed);
    }

    private void DestroyObject()
    {
        Destroy(this.gameObject);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.GetComponent<Enemy>())
        {
            collision.GetComponent<Enemy>().Health -= 1000;
        }
    }
}
