﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataBombAI : MonoBehaviour
{
    [HideInInspector]
    public DataBomb dataBomb;

    [SerializeField]
    GameObject _spread;

    float _timer;

    bool _bombSwitch;

    private void Start()
    {
        StartCoroutine(DoBlinking());
    }

    // Update is called once per frame
    void Update ()
    {
        _timer += Time.deltaTime;
    }

    IEnumerator DoBlinking()
    {
        yield return new WaitWhile(() => dataBomb == null);
        
        while (_timer < dataBomb.explosionTime)
        {
            if ((dataBomb.explosionTime - _timer) / dataBomb.explosionTime > 0.5f)
            {
                yield return new WaitForSeconds(0.2f);
            }
            else if ((dataBomb.explosionTime - _timer) / dataBomb.explosionTime > 0.25f)
            {
                yield return new WaitForSeconds(0.1f);
            }
            else
            {
                yield return new WaitForSeconds(0.05f);
            }
            _bombSwitch = !_bombSwitch;
            if (_bombSwitch)
            {
                this.GetComponent<SpriteRenderer>().sprite = dataBomb.blinkSprite;
            }
            else
            {
                this.GetComponent<SpriteRenderer>().sprite = dataBomb.normalSprite;
            }
        }
        if (_timer >= dataBomb.explosionTime)
        {
            AudioSource AS = this.gameObject.AddComponent<AudioSource>();
            GameObject go;
            for (int i = 0; i < 64; i++)
            {
                go = GameObject.Instantiate<GameObject>(_spread, this.transform.position, Quaternion.Euler(0, 0, (360.0f / (float)64) * i));
                go.GetComponent<SpreadAI>().empowerment = 100;
            }
            AS.PlayOneShot(dataBomb._attackSounds[0]);
            this.GetComponent<SpriteRenderer>().enabled = false;
            yield return new WaitForSeconds(1);

            Destroy(this.gameObject);
        }
        yield return null;
    }
}
