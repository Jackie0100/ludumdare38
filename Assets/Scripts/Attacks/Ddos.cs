﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Ddos : Attack
{
    Coroutine powerdrain;
    public override void DoAttack(Player player)
    {
        //base.DoAttack(player);

        player.IsSlowDownActive = !player.IsSlowDownActive;

        if (player.IsSlowDownActive)
        {
            player.Power -= _powerCost;
            powerdrain = player.StartCoroutine(DrainPower(player, 1));
            player.PlaySoundLoop(_attackSounds[UnityEngine.Random.Range(0, _attackSounds.Length)]);
        }
        else
        {
            player.StopCoroutine(powerdrain);
            player.StopSound(_attackSounds[UnityEngine.Random.Range(0, _attackSounds.Length)]);
        }
    }
}
