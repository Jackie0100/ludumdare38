﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Attack : IActionBarItem
{
    [SerializeField]
    Sprite _icon;
    [SerializeField]
    float _cooldownTimer;
    [SerializeField]
    float _cooldownTime;
    [SerializeField]
    protected GameObject _bulletObject;
    [SerializeField]
    protected float _powerCost;
    [SerializeField]
    string _name;
    [SerializeField, Multiline]
    string _describtion;
    [SerializeField]
    public AudioClip[] _attackSounds;
    [SerializeField]
    public bool isChoosen = false;

    public Sprite Icon
    {
        get
        {
            return _icon;
        }
        set
        {
            _icon = value;
        }
    }

    public float CooldownTimer
    {
        get
        {
            return _cooldownTimer;
        }
        set
        {
            _cooldownTimer = value;
        }
    }
    public float CooldownTime
    {
        get
        {
            return _cooldownTime;
        }
        set
        {
            _cooldownTime = value;
        }
    }

    public virtual void UseItem(Player player)
    {
        if (CooldownTimer <= 0 && IsUsable(player))
        {
            if (DoesHaveEnoughPower(player))
            {
                DoAttack(player);
                CooldownTimer = CooldownTime;
            }
        }
    }

    public virtual void DoAttack(Player player)
    {
        if (_attackSounds.Length > 0)
        {
            player.GetComponent<AudioSource>().PlayOneShot(_attackSounds[UnityEngine.Random.Range(0, _attackSounds.Length)]);
        }
        player.Power -= _powerCost;
    }

    public virtual IEnumerator DrainPower(Player player, float time)
    {
        while (true)
        {
            yield return new WaitForSeconds(time);
            player.Power -= _powerCost;
        }
        yield return null;
    }

    public bool DoesHaveEnoughPower(Player player)
    {
        return (player.Power - _powerCost > 0);
    }

    public virtual bool IsUsable(Player player)
    {
        return true;
    }

    public override string ToString()
    {
        return _name + ": " + _describtion;
    }
}
