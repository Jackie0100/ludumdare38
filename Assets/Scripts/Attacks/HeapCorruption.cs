﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[System.Serializable]
public class HeapCorruption : Attack
{
    public override void DoAttack(Player player)
    {
        base.DoAttack(player);

        Collider2D[] hitinfo = Physics2D.OverlapCircleAll(player.transform.position, 0.1f, 1);

        if (hitinfo.Length > 0)
        {
            hitinfo.First(h => h.GetComponent<Chip>() != null).GetComponent<Chip>().Corrupt();
        }
    }

    public override bool IsUsable(Player player)
    {
        Collider2D[] hitinfo = Physics2D.OverlapCircleAll(player.transform.position, 0.1f, 1);
        return hitinfo.Any(c => c.GetComponent<Chip>() != null);
    }
}
