﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Camera))]
public class CameraController : MonoBehaviour
{
    [SerializeField]
    Camera _targetCamera;
    [SerializeField]
    GameObject _targetObject;

	// Use this for initialization
	void Start () {
		if (_targetCamera == null)
        {
            _targetCamera = GetComponent<Camera>();
        }
        if (_targetObject == null)
        {
            Debug.LogError(this.name + " Has no target object assigned...");
        }
	}
	
	// Update is called once per frame
	void Update ()
    {
        //TODO: Consider if this camera movement is distacting
        //Vector3 movedir = new Vector3(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"), -10);

        //if (Vector3.Distance(Vector3.zero, movedir) > 1)
        //{
        //    movedir.Normalize();
        //}
        
        _targetCamera.transform.position = (_targetObject.transform.position + (Vector3.back * 10) /*+ (movedir * 25)*/);
	}
}
