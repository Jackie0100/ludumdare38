﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using System.Linq;
using System;

public class Player : MonoBehaviour
{
    [SerializeField]
    AudioClip[] _hitSounds;
    [SerializeField]
    PowerMeterUI _powerbarUI;
    [SerializeField]
    ActionBarUI _actionbarUI;
    [SerializeField]
    float _moveSpeed = 10;

    float _power = 200;

    float _maxPower = 200;

    bool _isSlowDownActive;
    bool _isInvissibleActive;

    Chip[] _chips;

    public UnityEvent OnPlayerDeath;
    public UnityEvent OnPlayerWin;




    public float Power
    {
        get
        {
            return _power;
        }

        set
        {
            if (value <= 0)
            {
                _power = 0;

                if (OnPlayerDeath != null)
                {
                    OnPlayerDeath.Invoke();
                }
            }
            else if (value > _maxPower)
            {
                _power = _maxPower;
            }
            else
            {
                _power = value;
            }

            _powerbarUI.SetBar(PowerPercentage);
        }
    }

    public float PowerPercentage
    {
        get
        {
            return Power / _maxPower;
        }
    }

    public bool IsSlowDownActive
    {
        get
        {
            return _isSlowDownActive;
        }

        set
        {
            _isSlowDownActive = value;
        }
    }

    public bool IsInvissibleActive
    {
        get
        {
            return _isInvissibleActive;
        }

        set
        {
            _isInvissibleActive = value;
        }
    }

    // Use this for initialization
    void Start ()
    {
        _chips = GameObject.Find("Map").transform.GetComponentsInChildren<Chip>();
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (_chips.All((Chip c) => { return c.IsCorrupted; }))
        {
            if (OnPlayerWin != null)
            {
                OnPlayerWin.Invoke();
            }
            return;
        }

        Vector3 movedir = new Vector3(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"), 0);

        if (Vector3.Distance(Vector3.zero, movedir) > 1)
        {
            movedir.Normalize();
        }

        this.transform.Translate(movedir * Time.deltaTime * _moveSpeed);

        if (Input.GetButtonDown("ActionBar1"))
        {
            _actionbarUI[0].UseItem(this);
        }
        if (Input.GetButtonDown("ActionBar2"))
        {
            _actionbarUI[1].UseItem(this);
        }
        if (Input.GetButtonDown("ActionBar3"))
        {
            _actionbarUI[2].UseItem(this);
        }
        if (Input.GetButtonDown("ActionBar4"))
        {
            _actionbarUI[3].UseItem(this);
        }

        RegainPower();
    }

    private void RegainPower()
    {
        Power += _chips.Count((Chip c) => { return c.IsCorrupted; }) * Time.deltaTime;
    }

    public void PlayHitSound()
    {
        AudioSource AS = this.gameObject.AddComponent<AudioSource>();
        AS.PlayOneShot(_hitSounds[UnityEngine.Random.Range(0, _hitSounds.Length)]);
    }

    public void PlaySound(AudioClip ac)
    {
        AudioSource AS = this.gameObject.AddComponent<AudioSource>();
        AS.PlayOneShot(ac);
        Destroy(AS, ac.length + 1);
    }

    public void PlaySoundLoop(AudioClip ac)
    {
        AudioSource AS = this.gameObject.AddComponent<AudioSource>();
        AS.loop = true;
        AS.clip = ac;
        AS.Play();
    }

    public void StopSound(AudioClip ac)
    {
        Destroy(this.GetComponents<AudioSource>().First(s => s.clip == ac));
    }
}
