﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public class PowerMeterUI : MonoBehaviour
{
    //TODO: Lerp on the fillamount in an update method?
    public void SetBar(float powerPercentage)
    {
        GetComponent<Image>().fillAmount = powerPercentage;
    }
}
