﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class ActionBarUI : MonoBehaviour
{
    [SerializeField]
    private AttackDataBase _attackDataBase;
    private ActionBarButtonUI[] _buttons;

    public ActionBarButtonUI this[int index]
    {
        get
        {
            return _buttons[index];
        }
        set
        {
            _buttons[index] = value;
        }
    }

    public ActionBarButtonUI[] Buttons
    {
        get
        {
            return _buttons;
        }

        set
        {
            _buttons = value;
        }
    }

    private void Start()
    {
        _buttons = new ActionBarButtonUI[this.transform.childCount];
        for (int i = 0; i < this.transform.childCount; i++)
        {
            _buttons[i] = this.transform.GetChild(i).GetComponent<ActionBarButtonUI>();
        }

        int buttonindex = 0;

        if (_attackDataBase.backDoorAttack.isChoosen)
        {
            _buttons[buttonindex].ActionBarItem = _attackDataBase.backDoorAttack;
            buttonindex++;
        }
        if (_attackDataBase.dataBombAttack.isChoosen)
        {
            _buttons[buttonindex].ActionBarItem = _attackDataBase.dataBombAttack;
            buttonindex++;
        }
        if (_attackDataBase.ddosAttack.isChoosen)
        {
            _buttons[buttonindex].ActionBarItem = _attackDataBase.ddosAttack;
            buttonindex++;
        }
        if (_attackDataBase.forkBombAttack.isChoosen)
        {
            _buttons[buttonindex].ActionBarItem = _attackDataBase.forkBombAttack;
            buttonindex++;
        }
        if (_attackDataBase.heapCorruptionAttack.isChoosen)
        {
            _buttons[buttonindex].ActionBarItem = _attackDataBase.heapCorruptionAttack;
            buttonindex++;
        }
        if (_attackDataBase.packageCorruptionAttack.isChoosen)
        {
            _buttons[buttonindex].ActionBarItem = _attackDataBase.packageCorruptionAttack;
            buttonindex++;
        }
        if (_attackDataBase.spreadAttack.isChoosen)
        {
            _buttons[buttonindex].ActionBarItem = _attackDataBase.spreadAttack;
            buttonindex++;
        }
        if (_attackDataBase.stackOverflowAttack.isChoosen)
        {
            _buttons[buttonindex].ActionBarItem = _attackDataBase.stackOverflowAttack;
            buttonindex++;
        }
        if (_attackDataBase.trojanAttack.isChoosen)
        {
            _buttons[buttonindex].ActionBarItem = _attackDataBase.trojanAttack;
            buttonindex++;
        }
    }
}