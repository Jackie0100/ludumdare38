﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MiniMapHandlerUI : MonoBehaviour
{
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
		if (Input.GetButtonDown("MiniMapButton"))
        {
            this.GetComponent<RawImage>().color = new Color(1, 1, 1, 0.35f);
        }
        if (Input.GetButtonUp("MiniMapButton"))
        {
            this.GetComponent<RawImage>().color = new Color(1, 1, 1, 0);
        }
    }
}
