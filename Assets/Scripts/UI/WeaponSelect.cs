﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WeaponSelect : MonoBehaviour
{
    [SerializeField]
    AttackDataBase _attackDataBase;
    [SerializeField]
    GameObject[] buttons;
    [SerializeField]
    Button _nextButton;

    int selectedamount;

    [SerializeField]
    Text _toolTip;

    private void OnEnable()
    {
        buttons[0].transform.GetChild(0).gameObject.SetActive(_attackDataBase.backDoorAttack.isChoosen);
        buttons[1].transform.GetChild(0).gameObject.SetActive(_attackDataBase.dataBombAttack.isChoosen);
        buttons[2].transform.GetChild(0).gameObject.SetActive(_attackDataBase.ddosAttack.isChoosen);
        buttons[3].transform.GetChild(0).gameObject.SetActive(_attackDataBase.forkBombAttack.isChoosen);
        buttons[4].transform.GetChild(0).gameObject.SetActive(_attackDataBase.heapCorruptionAttack.isChoosen);
        buttons[5].transform.GetChild(0).gameObject.SetActive(_attackDataBase.packageCorruptionAttack.isChoosen);
        buttons[6].transform.GetChild(0).gameObject.SetActive(_attackDataBase.spreadAttack.isChoosen);
        buttons[7].transform.GetChild(0).gameObject.SetActive(_attackDataBase.stackOverflowAttack.isChoosen);
        buttons[8].transform.GetChild(0).gameObject.SetActive(_attackDataBase.trojanAttack.isChoosen);
    }

    public void EnableWeaponry(int index)
    {


        switch (index)
        {
            case 1:
                _attackDataBase.backDoorAttack.isChoosen = !_attackDataBase.backDoorAttack.isChoosen;
                buttons[index - 1].transform.GetChild(0).gameObject.SetActive(_attackDataBase.backDoorAttack.isChoosen);
                break;
            case 2:
                _attackDataBase.dataBombAttack.isChoosen = !_attackDataBase.dataBombAttack.isChoosen;
                buttons[index - 1].transform.GetChild(0).gameObject.SetActive(_attackDataBase.dataBombAttack.isChoosen);
                break;
            case 3:
                _attackDataBase.ddosAttack.isChoosen = !_attackDataBase.ddosAttack.isChoosen;
                buttons[index - 1].transform.GetChild(0).gameObject.SetActive(_attackDataBase.ddosAttack.isChoosen);
                break;
            case 4:
                _attackDataBase.forkBombAttack.isChoosen = !_attackDataBase.forkBombAttack.isChoosen;
                buttons[index - 1].transform.GetChild(0).gameObject.SetActive(_attackDataBase.forkBombAttack.isChoosen);
                break;
            case 5:
                _attackDataBase.heapCorruptionAttack.isChoosen = !_attackDataBase.heapCorruptionAttack.isChoosen;
                buttons[index - 1].transform.GetChild(0).gameObject.SetActive(_attackDataBase.heapCorruptionAttack.isChoosen);
                break;
            case 6:
                _attackDataBase.packageCorruptionAttack.isChoosen = !_attackDataBase.packageCorruptionAttack.isChoosen;
                buttons[index - 1].transform.GetChild(0).gameObject.SetActive(_attackDataBase.packageCorruptionAttack.isChoosen);
                break;
            case 7:
                _attackDataBase.spreadAttack.isChoosen = !_attackDataBase.spreadAttack.isChoosen;
                buttons[index - 1].transform.GetChild(0).gameObject.SetActive(_attackDataBase.spreadAttack.isChoosen);
                break;
            case 8:
                _attackDataBase.stackOverflowAttack.isChoosen = !_attackDataBase.stackOverflowAttack.isChoosen;
                buttons[index - 1].transform.GetChild(0).gameObject.SetActive(_attackDataBase.stackOverflowAttack.isChoosen);
                break;
            case 9:
                _attackDataBase.trojanAttack.isChoosen = !_attackDataBase.trojanAttack.isChoosen;
                buttons[index - 1].transform.GetChild(0).gameObject.SetActive(_attackDataBase.trojanAttack.isChoosen);
                break;
        }

        selectedamount = 0;

        if (_attackDataBase.backDoorAttack.isChoosen)
        {
            selectedamount++;
        }
        if (_attackDataBase.dataBombAttack.isChoosen)
        {
            selectedamount++;
        }
        if (_attackDataBase.ddosAttack.isChoosen)
        {
            selectedamount++;
        }
        if (_attackDataBase.forkBombAttack.isChoosen)
        {
            selectedamount++;
        }
        if (_attackDataBase.heapCorruptionAttack.isChoosen)
        {
            selectedamount++;
        }
        if (_attackDataBase.packageCorruptionAttack.isChoosen)
        {
            selectedamount++;
        }
        if (_attackDataBase.spreadAttack.isChoosen)
        {
            selectedamount++;
        }
        if (_attackDataBase.stackOverflowAttack.isChoosen)
        {
            selectedamount++;
        }
        if (_attackDataBase.trojanAttack.isChoosen)
        {
            selectedamount++;
        }

        if (selectedamount > 4)
        {
            _nextButton.interactable = false;
        }
        else
        {
            _nextButton.interactable = true;
        }
    }

    public void ShowToolTip(int index)
    {
        switch (index)
        {
            case 1:
                _toolTip.text = _attackDataBase.backDoorAttack.ToString();
                break;
            case 2:
                _toolTip.text = _attackDataBase.dataBombAttack.ToString();
                break;
            case 3:
                _toolTip.text = _attackDataBase.ddosAttack.ToString();
                break;
            case 4:
                _toolTip.text = _attackDataBase.forkBombAttack.ToString();
                break;
            case 5:
                _toolTip.text = _attackDataBase.heapCorruptionAttack.ToString();
                break;
            case 6:
                _toolTip.text = _attackDataBase.packageCorruptionAttack.ToString();
                break;
            case 7:
                _toolTip.text = _attackDataBase.spreadAttack.ToString();
                break;
            case 8:
                _toolTip.text = _attackDataBase.stackOverflowAttack.ToString();
                break;
            case 9:
                _toolTip.text = _attackDataBase.trojanAttack.ToString();
                break;
        }
    }

    public void HideToolTip()
    {
        _toolTip.text = "Made by Jackie Engberg Christensen (@jacksendary) in 48 hours for the Ludum Dare Compo";
    }

    public void ShowTooManyItemsSelectedToolTip()
    {
        if (!_nextButton.interactable)
        {
            _toolTip.text = "Only a Maximum of 4 attacks can be used at a time, please disable some!";
        }
    }
}
