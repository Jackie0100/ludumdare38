﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class ActionBarButtonUI : MonoBehaviour
{
    public Image icon;
    public Image cooldownCounterOverlay;
    public Text text;
    private IActionBarItem actionBarItem;

    public IActionBarItem ActionBarItem
    {
        get
        {
            return actionBarItem;
        }
        set
        {
            icon.sprite = value.Icon;
            actionBarItem = value;
        }
    }

    private void Awake()
    {
        if (icon == null)
        {
            icon = this.transform.FindChild("Icon").GetComponent<Image>();
        }
        if (cooldownCounterOverlay == null)
        {
            cooldownCounterOverlay = this.transform.FindChild("CooldownOverlay").GetComponent<Image>();
        }
        if (text == null)
        {
            text = this.transform.FindChild("Text").GetComponent<Text>();
        }
    }

    private void Update()
    {
        //TODO: Consider having the cooldown hander in a better place?
        if (ActionBarItem != null)
        {
            if (ActionBarItem.CooldownTimer > 0)
            {
                ActionBarItem.CooldownTimer -= Time.deltaTime;
                if (ActionBarItem.CooldownTimer < 0)
                {
                    ActionBarItem.CooldownTimer = 0;
                }
            }
            if (!actionBarItem.DoesHaveEnoughPower(GameObject.FindGameObjectWithTag("Player").GetComponent<Player>()) || !actionBarItem.IsUsable(GameObject.FindGameObjectWithTag("Player").GetComponent<Player>()))
            {
                cooldownCounterOverlay.fillAmount = 1;
            }
            else if(ActionBarItem.CooldownTime != 0)
            {
                cooldownCounterOverlay.fillAmount = (ActionBarItem.CooldownTimer / ActionBarItem.CooldownTime);
            }
            else
            {
                cooldownCounterOverlay.fillAmount = 0;
            }
        }
    }

    public void UseItem(Player player)
    {
        if (ActionBarItem != null)
        {
            if (ActionBarItem.CooldownTimer <= 0)
            {
                ActionBarItem.UseItem(player);
            }
        }
    }
}