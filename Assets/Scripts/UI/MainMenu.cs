﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{


    public void ShowSelectWeapons()
    {

    }

    public void LoadLevel(int index)
    {
        SceneManager.LoadScene(index);
    }

    public void ShowLevelSelect()
    {

    }

    public void ShowMainMenu()
    {

    }

    public void ShutDown()
    {
        Application.Quit();
    }
}
