﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(PolygonCollider2D))]
public class CaptureArea : MonoBehaviour
{
    [SerializeField]
    float _captureTime = 10;

    float _captureCounter = 0;
    bool _iscapturing;

    public UnityEvent OnCaptureComplete;

    public float CaptureCounter
    {
        get
        {
            return _captureCounter;
        }

        set
        {
            if (value <= 0)
            {
                _captureCounter = 0;
            }
            else
            {
                _captureCounter = value;
                if (value >= _captureTime)
                {
                    if (OnCaptureComplete != null)
                    {
                        OnCaptureComplete.Invoke();
                    }
                }
            }
        }
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
		if (_iscapturing)
        {
            CaptureCounter += Time.deltaTime;
        }
        else if (CaptureCounter < _captureTime)
        {
            CaptureCounter -= (Time.deltaTime * 0.25f);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            _iscapturing = true;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            _iscapturing = false;
        }
    }
}
