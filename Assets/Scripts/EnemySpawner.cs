﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    [SerializeField]
    GameObject[] _enemies;
    [SerializeField]
    GameObject[] _spawnLoacations;

    Chip[] _chips;

    List<Enemy> _spawnedEnemies;

    // Use this for initialization
    void Start ()
    {
        _spawnLoacations = GameObject.FindGameObjectsWithTag("Chip");
        _chips = _chips = GameObject.Find("Map").transform.GetComponentsInChildren<Chip>();
        _spawnedEnemies = new List<Enemy>();
        StartCoroutine(SpawnHandler());
    }

    // Update is called once per frame
    void Update ()
    {
		
	}

    IEnumerator SpawnHandler()
    {
        int corruptedChips = 0;
        while(true)
        {
            corruptedChips = _chips.Count(c => c.IsCorrupted);

            for (int i = 0; i < _spawnedEnemies.Count; i++)
            {
                if (_spawnedEnemies[i] == null || _spawnedEnemies[i].AiState == AIStates.Converted)
                {
                    _spawnedEnemies.RemoveAt(i);
                    i--;
                }
            }

            if (_spawnedEnemies.Count() >= corruptedChips * 25)
            {
                yield return new WaitForEndOfFrame();

                continue;
            }

            GameObject goloc;

            foreach (GameObject go in _spawnLoacations)
            {
                if (IsPointSeenByCamera(go.transform.position, Camera.main))
                    continue;
                    
                goloc = GameObject.Instantiate<GameObject>(_enemies[Random.Range(0, _enemies.Length)], go.transform.position, Quaternion.identity, this.transform);
                _spawnedEnemies.Add(goloc.GetComponent<Enemy>());
            }
            yield return new WaitForSeconds(2);
        }
        yield return null;
    }

    private bool IsPointSeenByCamera(Vector3 pos, Camera cam)
    {
        Vector2 upper = cam.ViewportToWorldPoint(new Vector3(0, 0));
        Vector2 lower = cam.ViewportToWorldPoint(new Vector3(1, 1));

        return new Rect(upper, lower - upper).Contains(pos);
    }
}
