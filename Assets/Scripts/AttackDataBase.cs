﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackDataBase : ScriptableObject
{
    public BackDoor backDoorAttack;
    public DataBomb dataBombAttack;
    public Ddos ddosAttack;
    public ForkBomb forkBombAttack;
    public HeapCorruption heapCorruptionAttack;
    public PackageCorruption packageCorruptionAttack;
    public Spread spreadAttack;
    public StackOverflow stackOverflowAttack;
    public TrojanHorse trojanAttack;
}
