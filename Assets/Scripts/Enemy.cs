﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    [SerializeField]
    float _moveSpeed = 10;
    [SerializeField]
    AIStates _aiState;
    [SerializeField]
    Vector2 _guardPosition;
    [SerializeField]
    float _wanderDistance = 10;
    [SerializeField]
    List<Chip> _patrolPositions;

    [SerializeField]
    Vector2 _targetPosition;

    [SerializeField]
    float _health;

    [SerializeField]
    Player _chaseTarget;

    [SerializeField]
    Vector3 _actualPosition;

    [SerializeField]
    Sprite _convertedSprite;

    bool _isStunned = false;

    Coroutine _currentBehavior;
    Coroutine _subBehavior;


    public AIStates AiState
    {
        get
        {
            return _aiState;
        }

        set
        {
            if (_currentBehavior != null)
                StopCoroutine(_currentBehavior);
            if (_subBehavior != null)
                StopCoroutine(_subBehavior);

            switch (value)
            {
                case AIStates.Converted:
                    this.GetComponent<SpriteRenderer>().sprite = _convertedSprite;
                    _currentBehavior = StartCoroutine(ConvertedAI());
                    break;
                case AIStates.GuardPatrol:
                    _currentBehavior = StartCoroutine(GuardPatrolAI());
                    break;
                case AIStates.Guard:
                    _currentBehavior = StartCoroutine(GuardAI());
                    break;
                case AIStates.Patrol:
                    _currentBehavior = StartCoroutine(PatrolAI());
                    break;
                case AIStates.Chase:
                default:
                    _currentBehavior = StartCoroutine(ChaseAI());
                    break;

            }
            _aiState = value;
        }
    }

    public float Health
    {
        get
        {
            return _health;
        }
        set
        {
            if (value <= 0)
            {
                Destroy(this.gameObject);
            }
            _health = value;
        }
    }

    private void Awake()
    {
        _chaseTarget = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
        _patrolPositions = (List<Chip>)GameObject.Find("Map").transform.GetComponentsInChildren<Chip>().OrderBy((Chip item) => { return Random.Range(0, 10000); }).ToList<Chip>();
        _actualPosition = this.transform.position;
        AiState = (AIStates)Random.Range(0, 4);
    }

    // Use this for initialization
    void Start ()
    {

        //AiState = _aiState;
        //AiState = AIStates.GuardPatrol;
    }

    // Update is called once per frame
    void Update ()
    {
        if (_isStunned)
        {
            _moveSpeed = 0;
        }
		else if (_chaseTarget.IsSlowDownActive)
        {
            _moveSpeed = 5;
        }
        else
        {
            _moveSpeed = 10;
        }
    }

    public void StunEnemy(float time)
    {
        _isStunned = true;
        Invoke("UnStunEnemy", time);
    }

    public void UnStunEnemy()
    {
        _isStunned = false;
    }

    public void Convert(float Empowerment)
    {
        AiState = AIStates.Converted;
        Health = Empowerment;
        //this.GetComponent<SpriteRenderer>().color = new Color32(255, 0, 180, 255);
    }

    IEnumerator ChaseAI()
    {
        while (true)
        {
            if (_chaseTarget.IsInvissibleActive)
            {
                if (Vector2.Distance(_targetPosition, _actualPosition) <= 0.2f)
                {
                    _targetPosition = (Vector2)_actualPosition + (Random.insideUnitCircle * _wanderDistance);
                }
            }
            else
            {
                _targetPosition = _chaseTarget.transform.position;
            }
            _actualPosition = Vector3.MoveTowards(_actualPosition, _targetPosition, Time.deltaTime * _moveSpeed);
            if (!_chaseTarget.IsInvissibleActive)
            {
                this.transform.position = _actualPosition;
            }
            yield return new WaitForEndOfFrame();
        }
        yield return null;
    }

    IEnumerator GuardAI()
    {
        _guardPosition = _actualPosition;
        _targetPosition = _guardPosition + (Random.insideUnitCircle * _wanderDistance);

        while (true)
        {
            if (Vector2.Distance(_actualPosition, _chaseTarget.transform.position) <= 10 && !_chaseTarget.IsInvissibleActive)
            {
                if (_subBehavior == null)
                {
                    _subBehavior = StartCoroutine(ChaseAI());
                }
                yield return new WaitForEndOfFrame();

                continue;
            }
            else
            {
                if (_subBehavior != null)
                {
                    StopCoroutine(_subBehavior);
                    _subBehavior = null;
                }
            }

            if (Vector2.Distance(_targetPosition, _actualPosition) <= 0.2f)
            {
                _targetPosition = _guardPosition + (Random.insideUnitCircle * _wanderDistance);
            }

            _actualPosition = Vector3.MoveTowards(_actualPosition, _targetPosition, Time.deltaTime * _moveSpeed);
            if (!_chaseTarget.IsInvissibleActive)
            {
                this.transform.position = _actualPosition;
            }

            yield return new WaitForEndOfFrame();
        }
        yield return null;
    }

    IEnumerator GuardPatrolAI()
    {
        int index = 0;
        float timer = 0;
        _targetPosition = _patrolPositions[index].transform.position;
        index = (index + 1) % _patrolPositions.Count;

        while (true)
        {
            if (Vector2.Distance(_targetPosition, _actualPosition) <= 0.2f)
            {
                while (timer < 10)
                {
                    _guardPosition = _actualPosition;
                    if (Vector2.Distance(_targetPosition, _actualPosition) <= 0.2f)
                    {
                        _targetPosition = _guardPosition + (Random.insideUnitCircle * _wanderDistance);
                    }
                    timer += Time.deltaTime;
                    _actualPosition = Vector3.MoveTowards(_actualPosition, _targetPosition, Time.deltaTime * _moveSpeed);
                    if (!_chaseTarget.IsInvissibleActive)
                    {
                        this.transform.position = _actualPosition;
                    }
                    yield return new WaitForEndOfFrame();
                }
                _targetPosition = _patrolPositions[index].transform.position;
                index = (index + 1) % _patrolPositions.Count;
                timer = 0;
            }

            _actualPosition = Vector3.MoveTowards(_actualPosition, _targetPosition, Time.deltaTime * _moveSpeed);
            if (!_chaseTarget.IsInvissibleActive)
            {
                this.transform.position = _actualPosition;
            }
            yield return new WaitForEndOfFrame();
        }
        yield return null;
    }

    IEnumerator PatrolAI()
    {
        int index = 0;
        _targetPosition = _patrolPositions[index].transform.position;
        index = (index + 1) % _patrolPositions.Count;

        while (true)
        {
            if (Vector2.Distance(_chaseTarget.transform.position, _actualPosition) <= 10 && !_chaseTarget.IsInvissibleActive)
            {
                if (_subBehavior == null)
                {
                    _subBehavior = StartCoroutine(ChaseAI());
                }
                yield return new WaitForEndOfFrame();

                continue;
            }
            else
            {
                if (_subBehavior != null)
                {
                    StopCoroutine(_subBehavior);
                    _subBehavior = null;
                }
            }

            if (Vector2.Distance(_targetPosition, _actualPosition) <= 0.2f)
            {
                _targetPosition = _patrolPositions[index].transform.position;
                index = (index + 1) % _patrolPositions.Count;
            }

            _actualPosition = Vector3.MoveTowards(_actualPosition, _targetPosition, Time.deltaTime * _moveSpeed);
            if (!_chaseTarget.IsInvissibleActive)
            {
                this.transform.position = _actualPosition;
            }
            yield return new WaitForEndOfFrame();
        }
        yield return null;
    }

    IEnumerator ConvertedAI()
    {
        _guardPosition = _actualPosition;
        Collider2D[] hitinfo;
        _targetPosition = _guardPosition + (Random.insideUnitCircle * _wanderDistance);

        while (true)
        {
            hitinfo = Physics2D.OverlapCircleAll(_actualPosition, 10, 1);

            if (hitinfo.Length > 0)
            {
                float dist = 100;
                Vector2 _target = new Vector2();
                foreach (Collider2D col in hitinfo)
                {
                    if (col.GetComponent<Enemy>() == null)
                        continue;
                    if (col.GetComponent<Enemy>().AiState == AIStates.Converted)
                        continue;
                    if (col.name.Contains("VirusSpread"))
                        continue;


                    Enemy enemy = col.GetComponent<Enemy>();


                    if (Vector2.Distance(col.transform.position, this.transform.position) < dist)
                    {
                        dist = Vector2.Distance(col.transform.position, this.transform.position);
                        _target = col.transform.position;
                        _targetPosition = _target;
                    }
                }
            }

            if (Vector2.Distance(_targetPosition, _actualPosition) <= 0.2f)
            {
                _targetPosition = _guardPosition + (Random.insideUnitCircle * _wanderDistance);
            }

            _actualPosition = Vector3.MoveTowards(_actualPosition, _targetPosition, Time.deltaTime * _moveSpeed);

            if (!_chaseTarget.IsInvissibleActive)
            {
                this.transform.position = _actualPosition;
            }

            yield return new WaitForEndOfFrame();
        }
        yield return null;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            collision.transform.GetComponent<Player>().Power -= 5;
            collision.transform.GetComponent<Player>().PlayHitSound();
            Health = 0;
        }
        if (collision.gameObject.tag == "Enemy")
        {
            if (AiState == AIStates.Converted && collision.transform.GetComponent<Enemy>().AiState != AIStates.Converted)
            {
                Health -= 100;
                collision.transform.GetComponent<Enemy>().Convert(Health);
            }
        }
    }
}

public enum AIStates
{
    Guard, Patrol, Chase, GuardPatrol, Converted,
}